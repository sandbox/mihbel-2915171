INTRODUCTION
------------

The module provides a tool that allows to replace 
taxonomy terms in the fields of nodes and other "fieldable" 
entities. Also, with help of this tool it is possible 
to replace one taxonomy term with multiple (split the taxonomy 
terms) and multiple taxonomy terms into one (merge the taxonomy terms).


INSTALLATION
------------

Install as usual, see
https://drupal.org/documentation/install/modules-themes/modules-7 
for further information.